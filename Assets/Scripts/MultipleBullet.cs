﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleBullet : Bullet {

	public int maxAmmo = 2;
	Cartridge cartridge;
	public int timeToExplode = 0;
	private float currentTime = 0f;

	void Awake ()
	{   
		cartridge = GameObject.Find("Weapons").GetComponent<Weapons>().cartridges[0];
	}

	protected new virtual void Update(){
		base.Update ();
		if (shooting) {
			currentTime += Time.deltaTime;
			if (currentTime > timeToExplode) {
				ShotMiniBullets ();
			}
		} else {
			currentTime = 0f;
		}
	}

	void ShotMiniBullets()
	{
		Debug.Log("Shot mini bullets");
		float z = transform.rotation.eulerAngles.z;
		cartridge.GetBullet().Shot(transform.position, 90 + z);
		cartridge.GetBullet().Shot(transform.position, -90 + z);
		cartridge.GetBullet().Shot(transform.position,  z);
		cartridge.GetBullet().Shot(transform.position, -z);
		cartridge.GetBullet().Shot(transform.position, 45 + z);
		cartridge.GetBullet().Shot(transform.position, -45 + z);
		cartridge.GetBullet().Shot(transform.position, 135 + z);
		cartridge.GetBullet().Shot(transform.position, -135 + z);

		Reset ();
	}
}
