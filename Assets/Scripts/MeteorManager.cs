﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorManager : MonoBehaviour {

	public Transform launchPos;
	public float xVariation;
	public Transform creationPos;
	public float timeLaunch;


	public GameObject[] meteorPrefabs;
	private MeteorCache[] meteorCaches;
	private float currentTime;



	// Use this for initialization
	void Awake () {
		Vector3 creationPosMeteor = creationPos.position;
		meteorCaches = new MeteorCache[meteorPrefabs.Length];

		for (int i = 0; i < meteorPrefabs.Length; i++) {
			//Big Meteors;
			meteorCaches[i] = new MeteorCache (meteorPrefabs[i], creationPosMeteor, creationPos, 30);

			creationPosMeteor.y += 1;
		}
	}

	void Update(){
		currentTime += Time.deltaTime;

		if (currentTime > timeLaunch) {
			meteorCaches [Random.Range(0, meteorCaches.Length)].GetMeteor ().LaunchMeteor (launchPos.position, new Vector2 (Random.Range(-5, 5), Random.Range(-5, -1)));
			currentTime -= timeLaunch;
		}
	}

}