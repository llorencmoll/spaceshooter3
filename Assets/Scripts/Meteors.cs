﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteors : MonoBehaviour {

	public bool launched;
	public Vector2 moveSpeed;
	protected float rotationSpeed;

	public ParticleSystem particle;

	public GameObject graphicsmeteor;

	private Collider2D meteorCollider;


	public Vector3 iniPos;

	void Start(){
		iniPos = transform.position;
		meteorCollider = GetComponent<Collider2D> ();
	}

	public void LaunchMeteor(Vector3 position,Vector2 direction){
		transform.position = position;
		launched = true;
		transform.rotation = Quaternion.Euler(0, 0, 0);
		moveSpeed = direction;
		//rotationSpeed = rotation;
		graphicsmeteor.gameObject.SetActive (true);
		meteorCollider.enabled = true;
	}

	// Update is called once per frame
	protected void Update () {
		if (launched) {
			transform.Translate (moveSpeed.x*Time.deltaTime, moveSpeed.y * Time.deltaTime, 0);
			transform.Rotate (0, 0, rotationSpeed * Time.deltaTime);
		}
	}

	protected void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Bullet") {
			Explode ();
		} else if (other.tag == "Finish") {
			Reset ();
		}
	}

	protected void Reset(){
		transform.position = iniPos;
		launched = false;
	}

	protected void Explode(){
		meteorCollider.enabled = false;
		launched = false;
		graphicsmeteor.gameObject.SetActive (false);
		particle.Play ();
		Invoke ("Reset", 1);

	}
}