﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeBullet : Bullet {

	public int maxAmmo = 2;
	Cartridge cartridge;
	public int timeToExplode = 1;
	private float currentTime = 0f;

	void Awake ()
	{   
		cartridge = GameObject.Find("Weapons").GetComponent<Weapons>().cartridges[0];
	}

	protected new virtual void Update(){
		base.Update ();
		if (shooting) {
			currentTime += Time.deltaTime;
			//transform.Translate (0, speed * Time.deltaTime, 0);
			if (currentTime > timeToExplode) {
				float z = transform.rotation.eulerAngles.z;
				cartridge.GetBullet().Shot(transform.position, -90 + z);
				//transform.Translate (speed * Time.deltaTime, 0, 0);
			}
			Reset ();
		}
	}
}