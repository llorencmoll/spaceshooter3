﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorCache {
	private Meteors[] meteors;
	private int currentMeteor = 0;


	public MeteorCache(GameObject prefabMeteor, Vector3 position, Transform parent, int numMeteors){
		meteors = new Meteors[numMeteors];

		Vector3 tmpPosition = position;
		for(int i=0;i<numMeteors;i++){
			meteors[i] = GameObject.Instantiate (prefabMeteor, tmpPosition, Quaternion.identity, parent).GetComponent<Meteors>();
			meteors[i].name = prefabMeteor.name+"_meteor_" + i;
			tmpPosition.x += 1;
		}
	}

	public Meteors GetMeteor(){
		if (currentMeteor > meteors.Length - 1) {
			currentMeteor = 0;
		}

		return meteors [currentMeteor++];
	}
}